	extern crate openal;

	use std::i16;
	use std::f64::consts::PI;
	use std::num::FloatMath;
	use std::io::timer::sleep;
	use std::time::duration::Duration;
	use openal::al;

	// A module named `openal_wrapper`
	mod openalc_wrapper {

		extern crate openal;
		use openal::alc;

		pub struct Openal {
			device: openal::alc::Device,
			context: openal::alc::Context,
		}	

		impl Openal {
			pub fn init() -> Result<Openal, String> {

				let device  = match alc::Device::open(None) {
				    Some(dev) => dev,

				    None => {
					return Err("Couldn't open device".to_string());
				    }
				};

				let context = match device.create_context(&[0i32, ..0]) {
				    Some(context) => context,

				    None => {
					// This wouldn't be needed if device followed RAII
					device.close();
					return Err("Couldn't create context".to_string());
				    }
				};

				context.make_current();

				Ok(Openal {
				    device: device,
				    context: context,
				})
			}
		}

		impl Drop for Openal {
			fn drop(&mut self) {
				//self.context.destroy();
				self.device.close();
			}
		}	
	}

	/*
	 * Main
	 */
	fn main() {
		println!("OpenAL test");

		// init
		let wrapper = openalc_wrapper::Openal::init();
		match wrapper{
			Ok(_) => println!("Init successful"),
			Err(e) => println!("Error: {}", e),
		};

		// play
		play_sinwave();

	}



	/*
	 * Play
	 */
	fn play_sinwave() {
		  let buffer = al::Buffer::gen();
		  let source = al::Source::gen();

		  let sample_freq = 44100.0;
		  let tone_freq = 440.0;
		  let duration = 3.0;
		  let num_samples = (sample_freq * duration) as uint;

		  let samples: Vec<i16> = Vec::from_fn(num_samples, |x| {
		    let t = x as f64;
		    ((tone_freq * t * 2.0 * PI / sample_freq).sin() * (i16::MAX - 1) as f64) as i16
		  });

		  unsafe { buffer.buffer_data(al::Format::FormatMono16, samples.as_slice(), sample_freq as al::ALsizei) };

		  source.queue_buffer(&buffer);
		  source.play();

		  sleep(Duration::milliseconds((duration * 1000.0) as i64));
	}

