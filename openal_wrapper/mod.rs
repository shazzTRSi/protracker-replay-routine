// A module named `openal_wrapper`
extern crate openal;
use openal::alc;

pub struct Openal {
	device: openal::alc::Device,
	context: openal::alc::Context,
}

impl Openal {
	pub fn init() -> Result<Openal, String> {

		let device  = match alc::Device::open(None) {
		    Some(dev) => dev,

		    None => {
			return Err("Couldn't open device".to_string());
		    }
		};

		let context = match device.create_context(&[0i32, ..0]) {
		    Some(context) => context,

		    None => {
			// This wouldn't be needed if device followed RAII
			device.close();
			return Err("Couldn't create context".to_string());
		    }
		};

		context.make_current();

		Ok(Openal {
		    device: device,
		    context: context,
		})
	}
}

impl Drop for Openal {
	fn drop(&mut self) {
		//self.context.destroy();
		self.device.close();
	}
}
