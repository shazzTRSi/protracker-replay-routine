#![feature(slicing_syntax)]

extern crate openal;

use std::io::File;
use std::io::BufferedReader;
use std::cmp::max;
use std::mem::uninitialized;

use std::i8;
use std::f64::consts::PI;
use std::num::FloatMath;
use std::io::timer::sleep;
use std::time::duration::Duration;
use openal::al;

mod openal_wrapper;


//debug : std::io::stdio::stdout_raw().write_str("read pattern");

struct TrackerModule {
  title: String,
  samples: Vec<Sample>,
  positions: u8,
  pattern_table: Vec<u8>,
  // 128 patterns max per module
  patterns_data: Vec<Pattern>,
}

struct Sample {
  name : String,
  length : u16,
  finetune : u8,
  volume : u8,
  repeat_offset : u16,
  repeat_length : u16,
  samples_data: Vec<i8>,
}

struct Pattern {
  // 4 channels per pattern
  channels: Vec<Channel>
}

struct Channel {
  // 64 cells per channel
  cells: Vec<Cell>
}

struct Cell {
  sample: u8,
  period: u16,
  effect: u8,
  params: u8
}


fn extract_string(data: Vec<u8>) -> Result<String, String> {
  let raw_str = String::from_utf8(data);
  match raw_str {
    Err(e) => Err(format!("couldn't extract string: {}" , e)),
    Ok(s) => match s.as_slice().find_str("\0") {
      Some(pos) => Ok(s.as_slice().slice_to(pos).to_string()),
      None => Ok(s),
      },
    }
}

#[allow(unused_must_use)]
fn load_module(filename: String) -> Result<TrackerModule, String>  {

  let path = Path::new(filename);
  let mut reader = BufferedReader::new(File::open(&path));
  let mod_title = match reader.read_exact(20) {
    Ok(vec) => Ok(extract_string(vec).unwrap()),
    Err(e) => Err(e),
  };

  println!("module title: {}" , mod_title);

  let mut samples: Vec<Sample> = Vec::with_capacity(31);
  for n in range(0u, 31) {

    println!("Reading sample: {}" , n);

    let sample_name_res = match reader.read_exact(22) {
      Ok(vec) => Ok(extract_string(vec).unwrap()),
      Err(e) => Err(e),
    };
    let sample_name = sample_name_res.unwrap();
    let sample_len = reader.read_be_u16().unwrap();
    let sample_finetune = reader.read_byte().unwrap();
    let sample_vol = reader.read_byte().unwrap();
    let sample_repoffset = reader.read_be_u16().unwrap();
    let sample_replen = reader.read_be_u16().unwrap();

    println!("Creating sample '{}' {} of len {}.w ({}.b finetune {} vol {} repeat_length {}.w ({}.b) repeat_offset {}.w ({}.b)" ,
    n, sample_name, sample_len, sample_len*2, sample_finetune, sample_vol, sample_replen, sample_replen*2, sample_repoffset, sample_repoffset*2);

    let a_sample : Sample = Sample {
      name: sample_name,
      length: sample_len,
      finetune: sample_finetune,
      volume: sample_vol,
      repeat_offset: sample_repoffset,
      repeat_length: sample_replen,
      samples_data: vec![],
    };

    samples.push(a_sample);
  }

  let nb_pos = reader.read_byte().unwrap();
  println!("module positions: {}" , nb_pos);

  //can be safely ignored
  reader.read_byte();

  let pattern_table = reader.read_exact(128).unwrap();
  let mut nb_patts = 0u;
  for &v in pattern_table.iter() {
    nb_patts = max(nb_patts, 1 + v.to_uint().unwrap());
  }
  println!("module has {} patterns: {}" , nb_patts, pattern_table);

  // should be M.K.
  let signature = reader.read_exact(4).unwrap();
  println!("check tracker signature: '{}'" , String::from_utf8(signature) );

  // read patterns data
  let mut patterns_data: Vec<Pattern> = Vec::with_capacity(nb_patts);
  for p in range(0u, nb_patts) {

    let mut cells_ch1: Vec<Cell> = Vec::with_capacity(64);
    let mut cells_ch2: Vec<Cell> = Vec::with_capacity(64);
    let mut cells_ch3: Vec<Cell> = Vec::with_capacity(64);
    let mut cells_ch4: Vec<Cell> = Vec::with_capacity(64);

    for r in range(0u, 64) {

      let cell1 = reader.read_be_u32().unwrap();
      cells_ch1.push(Cell {
                        sample: (((cell1 >> 28) << 4) | ( (cell1 & 0xF000) >> 12)).to_u8().unwrap(),
                        period: ((cell1 >> 16) & 0x0FFF).to_u16().unwrap(),
                        effect: ((cell1 & 0xF00) >> 8).to_u8().unwrap(),
                        params: (cell1 & 0x0FF).to_u8().unwrap() });

      let cell2 = reader.read_be_u32().unwrap();
      cells_ch2.push(Cell {
                          sample: (((cell2 >> 28) << 4) | ( (cell2 & 0xF000) >> 12)).to_u8().unwrap(),
                          period: ((cell2 >> 16) & 0x0FFF).to_u16().unwrap(),
                          effect: ((cell2 & 0xF00) >> 8).to_u8().unwrap(),
                          params: (cell2 & 0x0FF).to_u8().unwrap() });

      let cell3 = reader.read_be_u32().unwrap();
      cells_ch3.push(Cell {
                        sample: (((cell3 >> 28) << 4) | ( (cell3 & 0xF000) >> 12)).to_u8().unwrap(),
                        period: ((cell3 >> 16) & 0x0FFF).to_u16().unwrap(),
                        effect: ((cell3 & 0xF00) >> 8).to_u8().unwrap(),
                        params: (cell3 & 0x0FF).to_u8().unwrap() });

      let cell4 = reader.read_be_u32().unwrap();
      cells_ch4.push(Cell {
                      sample: (((cell4 >> 28) << 4) | ( (cell4 & 0xF000) >> 12)).to_u8().unwrap(),
                      period: ((cell4 >> 16) & 0x0FFF).to_u16().unwrap(),
                      effect: ((cell4 & 0xF00) >> 8).to_u8().unwrap(),
                      params: (cell4 & 0x0FF).to_u8().unwrap() });

    }
    let ch1 : Channel = Channel { cells : cells_ch1 };
    let ch2 : Channel = Channel { cells : cells_ch2 };
    let ch3 : Channel = Channel { cells : cells_ch3 };
    let ch4 : Channel = Channel { cells : cells_ch4 };

    let pat_array: Vec<Channel> = vec![ch1, ch2, ch3, ch4];
    let a_patt : Pattern = Pattern { channels : pat_array};
    patterns_data.push(a_patt);

  }
  // find non empty samples
  let mut nb_samples = 0u;
  for s in samples.iter() {
    if s.length != 0 { nb_samples = nb_samples+1; }
  }

  println!("Reading {} sample data", nb_samples);

  // read sample data
  for sd in range(0u, nb_samples) {

    let sdata: Vec<i8> = Vec::new();
    let udata = reader.read_exact( (samples[sd].length*2).to_uint().unwrap());

    // translate u8 to i8
    samples[sd].samples_data = udata.unwrap().map_in_place(|i| i as i8);
  }

  // all done !
  Ok(TrackerModule {
    title: mod_title.unwrap(),
    samples: samples,
    positions: nb_pos,
    pattern_table: pattern_table,
    patterns_data: patterns_data,
  })

}


fn main() {

  let filename = "8bb_-_darkness.mod".to_string();
  println!("Reading {}", filename);
  let module : TrackerModule = load_module(filename).unwrap();

  // read some data from the module struct
  let ref first_patt = module.patterns_data[0];
  let ref first_chn = first_patt.channels[0];
  let ref first_cell = first_chn.cells[32];

  println!("means: sample {} period {} effect type {} effect prm {}", first_cell.sample, first_cell.period, first_cell.effect, first_cell.params );

  // init
  let wrapper = openal_wrapper::Openal::init();
  match wrapper{
    Ok(_) => println!("Init successful"),
    Err(e) => println!("Error: {}", e),
  };

  /*
  let nb = 5u;
  let ref data = module.samples[nb].samples_data;

  let buffer = al::Buffer::gen();
  let source = al::Source::gen();
  source.set_looping(false);

  let vol : f32 = module.samples[nb].volume.to_f32().unwrap();
  source.set_gain(vol/64.0);
  let sample_freq: f64 = 8287.0;
  let note = "?".to_string();

  println!("Let's play sample {} '{}' at freq {} / note {}, repeating at {}, at vol {}->{}", nb, module.samples[nb].name, sample_freq, note, module.samples[nb].repeat_offset, vol,  vol/64.0f32 );

  // don't know why it is not FormatMono8...
  unsafe { buffer.buffer_data(al::Format::FormatMono16, data.as_slice(), sample_freq as al::ALsizei) };
  println!("Buffer depth {} freq {} channels {}", buffer.get_bits(), buffer.get_frequency(), buffer.get_channels());
  source.queue_buffer(&buffer);

  if module.samples[nb].repeat_length > 2   {

    let buffer2 = al::Buffer::gen();
    let start : uint = (module.samples[nb].repeat_offset*2).to_uint().unwrap();
    let stop  : uint = start + (module.samples[nb].repeat_length*2).to_uint().unwrap();

    println!("creating loop buffer from {} to {}", start, stop);

    let repbuf = data[start..stop];
    unsafe { buffer2.buffer_data(al::Format::FormatMono16, repbuf, sample_freq as al::ALsizei) };
    for sd in range(0u, 100) {
      source.queue_buffer(&buffer2);
    }
  }

  source.play();
  sleep(Duration::milliseconds((3.0f64 * 1000.0) as i64));
  source.stop();
  */


  //let source = al::Source::gen();

  println!("Let's play one pattern roughly");
  let ref vect =  module.patterns_data[0].channels[0].cells;
  for acell in vect.iter() {
    play_sample(acell.sample.to_int().unwrap()-1, acell.period, &module/*, &source*/);
  }

}

fn play_sample(samp_nb: int, period: u16, module : &TrackerModule/*, source : &al::Source*/) {

  if samp_nb >= 0  {

    let source = al::Source::gen();


    let smp_nb : uint = samp_nb.to_uint().unwrap();
    let buffer = al::Buffer::gen();

    let ref data = module.samples[smp_nb].samples_data;

    let vol : f32 = module.samples[smp_nb].volume.to_f32().unwrap();
    source.set_gain(vol/64.0);
    let sample_freq: f64 = 8287.0*428.0/(period.to_f64().unwrap());

    println!("Sample {} at period {} freq {}", smp_nb, period, sample_freq);

    unsafe { buffer.buffer_data(al::Format::FormatMono16, data.as_slice(), sample_freq as al::ALsizei) };
    source.queue_buffer(&buffer);

    if module.samples[smp_nb].repeat_length > 2   {

      let buffer2 = al::Buffer::gen();
      let start : uint = (module.samples[smp_nb].repeat_offset*2).to_uint().unwrap();
      let stop  : uint = start + (module.samples[smp_nb].repeat_length*2).to_uint().unwrap();

      //println!("creating loop buffer from {} to {}", start, stop);

      let repbuf = data[start..stop];
      unsafe { buffer2.buffer_data(al::Format::FormatMono16, repbuf, sample_freq as al::ALsizei) };
      for sd in range(0u, 1) {
        source.queue_buffer(&buffer2);
      }
    }
    //if !source.is_playing() {
      println!("let's play");
      source.play();
      sleep(Duration::milliseconds((3.0f64 * 100.0) as i64));
      source.stop();
    //}
  }
  else {
    println!("Nothing to play");
    sleep(Duration::milliseconds((3.0f64 * 100.0) as i64));
  }


}
